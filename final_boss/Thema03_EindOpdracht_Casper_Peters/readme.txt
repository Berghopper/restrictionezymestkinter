#########################################################################
### t3.py GUI+CLI versie 2												#
# fasta checks geoptimaliseerd (nogmaals)								#
# hard dict gebruiken als enzymes niet wordt meegegeven (CLI)			#
# met GUI wordt iedere run niet meer geprint							#
# pop-up window toegevoegd na het runnen van restriction bij gebruik GUI#
# errors worden gegeven bij het uitvoeren van het programma, i.p.v af	#
# te sluiten probeert het programma een zo goed mogelijk resultaat aan	#
# te bieden.															#
# csv file hoeft niet meegegeven te worden!								#
#########################################################################

################
##readme GUI
Om de gui te starten, roep de volgende command aan;
python3 restriction_GUI_v2.py

het fasta van de casus heet 'combined.fa'
wanneer een fasta verkeerde syntax heeft, werkt het programma nog wel,
maar kunnen je resultaten niet accuraat zijn.

fout (meerdere headers, foutieve DNA sequenties):
>1
GATCGATC
>3
>5
bla
>2
GATCGATCGATC
>3
GATC
>4
GATC

goed:

>1
GATCGATCGAGAC
>2
GATCGATCGATC
>3
GATC
>4
GATC

wanneer er een foutief fasta wordt meegegeven,geeft het programma een
error pop-up waaraan je kunt zien wat er fout is gegaan.
Wees er dus zeker van het fasta nog even te controleren!

als je het programma met het casus runt, dan krijg je het volgende:
patient 1 en 3 hebben mutant type
patient 2 heeft wild type

################
##readme CLI

usage: ./t3.py -f [FASTA] -ef [ENZYMEFILE] -e [ENZYMES]
de '-ef' optie hoeft niet meer sinds versie 2, als '-ef' niet wordt
meegegeven, gebruikt het programma een hard-coded dictionary.
Om de lijst van enzymen weer te geven, geef een foutief enzym in (onzin)
met meerde enzymen, splitsen met spatie:
./t3.py -f test_multiple.fasta -ef enzymes.csv -e MboI TaqI


test fastas:
###test_single.fasta
input:
./t3.py -f test_single.fasta -ef enzymes.csv -e MboI
wat er uit zou moeten komen (kan een andere volgorde zijn):
>1
[523.48,523.48]
>2
[523.48,523.48,523.48]
>3
[523.48]
>4
[523.48]
###test_multiple.fa
input:
./t3.py -f test_multiple.fa -ef enzymes.csv -e HinfI mst2 TaqI EcoRV BamHI EcorI EcoRII MboI Sau3A PstI HaeIII XbaI NotI HindIII
wat er uit zou moeten komen (kan een andere volgorde zijn):
>1
[564.49, 610.55, 658.61]
>2
[151.13, 0, 0, 769.71, 1109.02, 785.7099999999999, 658.61]



het fasta van de casus heet 'combined.fa'
om deze volledig te runnen gebruik de volgende line:
input:
./t3.py -f combined.fa -ef enzymes.csv -e HinfI mst2 TaqI EcoRV BamHI EcorI EcoRII MboI Sau3A PstI HaeIII XbaI NotI HindIII
output:
the program ran correctly!
runtime= 0.0036 seconds

>Cf wildtype
[8743.020000000002, 2491.28, 5874.340000000002, 13492.33, 1454.29, 1198.1100000000001, 0, 14639.459999999997]
>Cf mutant
[824.76, 8185.500000000004, 16556.13, 7004.430000000002, 111.1, 0, 9626.860000000004, 5156.760000000002]
>patient_1
[824.76, 8185.500000000004, 16556.13, 7004.430000000002, 111.1, 0, 9626.860000000004, 5156.760000000002]
>patient_2
[8743.020000000002, 2491.28, 5874.340000000002, 13492.33, 1454.29, 1198.1100000000001, 0, 14639.459999999997]
>patient_3
[824.76, 8185.500000000004, 16556.13, 7004.430000000002, 111.1, 0, 9626.860000000004, 5156.760000000002]
