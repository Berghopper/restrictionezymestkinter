#!/usr/bin/env python3
"""
program that digests DNA with a range of enzymes
"""

# imports
from __future__ import print_function
import sys
import re
import argparse
import time
from tkinter import Tk
from tkinter import Button
from tkinter import Label



# constants
IUPAC_CHARS = {
    "A": "A",
    'G': 'G',
    'C': 'C',
    'T': 'T',
    'N': '[ATGC]',
    'R': '[AG]',
    'Y': '[CT]',
    'S': '[GC]',
    'W': '[AT]',
    'K': '[GT]',
    'M': '[AC]',
    'B': '[CGT]',
    'D': '[AGT]',
    'H': '[ACT]',
    'V': '[ACG]',
    '.': ' ',
    '-': ' '
}

MOLMASS_DNA = {
    'A': 135.13,
    'C': 111.10,
    'G': 151.13,
    'T': 126.12
}

HARD_DICT_ENZYMES = {
    'EcoRV': ('GATATC', '3'),
    'HinfI': ('GANTCA', '1'),
    'MboI': ('GATC', '0'),
    'XbaI': ('TCTAGA', '1'),
    'EcoRII': ('CCWGG', '0'),
    'HaeIII': ('GGCC', '2'),
    'PstI': ('CTGCAG', '4'),
    'Sau3A': ('GATC', '0'),
    'EcorI': ('GAATTC', '1'),
    'HindIII': ('AAGCTT', '1'),
    'mst2': ('CCTNAGG', '2'),
    'BamHI': ('GGATCC', '1'),
    'NotI': ('GCGGCCGC', '2'),
    'TaqI': ('TCGA', '1')
}

DNA_PATTERN = "[^ACGTacgt]"
CSV_PATTERN = ".+[;][AGCTNRYSWKMBDHV.-]+[;][0-9](\n)"


# functions
def fastacheck(line):
    """
    checks if given fileline is a correct dna string,
    returns True or False depending on outcome
    """
    # booly is true until proven otherwise
    booly = True
    if line.startswith(">"):
        pass
    else:
        # replace enters with nothing to avoid confusion for the
        # check
        line = line.replace("\n", "")
        # when there's something else than ACGT in line return
        # false.
        # False gets returned immediatly to avoid state being set
        # to true with the next line
        if re.search(DNA_PATTERN, line) != None:
            return False
        else:
            booly = True
    return booly


def csv_check(csvfile, line):
    """
    checks if given fileline is a csv pattern
    returns True or False depending on outcome
    """
    # if file has correct extension, check file
    if csvfile.endswith(".csv"):
        # booly is true until proven otherwise
        booly = True
        # for each line, check for any odd chars
        if bool(re.search(CSV_PATTERN, line)) == True:
            booly = True
        # when there's something else than the pattern in line,
        # return false.
        # False gets returned immediatly to avoid state being set
        # to true with the next line
        else:
            booly = False
    else:
        booly = False
    return booly


def fasta_open(fastafile):
    """
    opens a fasta and returns a dict (keys=header, values=sequences)
    """
    fragments = {}
    errors = []
    #check if extension is correct
    if not fastafile.endswith(".fa") or fastafile.endswith(".fasta"):
        errors.append('wrong fasta file or no file!,'+\
        ' must be ".fa" or ".fasta"')
        return (fragments, errors)
    else:
        opened = open(fastafile)
        header = False
        dna = False

        count = 1
        for line in opened:
            # if fastaline is not correct, append to errors,
            # and pass current line
            if not fastacheck(line):
                #prevent unboundlocal error if all lines dont match
                headerline = ""
                valueline = ""
                #check
                errors.append("line "+str(count)+\
                "|dna seq is incorrect; "+line.strip("\n"))
                pass
            # check if line is header, set header state to true and
            # dna to false
            elif line.startswith(">") and not header:
                #if headerline and valueline have not yet been defined
                #it is the first header, else the variables will be added to
                #the dict and be reset
                #also a check for incomplete pairs
                try:
                    if not bool(valueline) or not bool(headerline):
                        valueline = ""
                        headerline = ""
                    else:
                        fragments.update({headerline:valueline})
                except UnboundLocalError:
                    pass
                headerline = ""
                valueline = ""
                header = True
                dna = False
                headerline = line.strip("\n").strip(" ")
            # if line begins with header but there was a header before it,
            # append to errors and replace the header
            elif line.startswith(">") and header:
                errors.append("line "+str(count)+\
                "|reoccuring header, skipping last header; "+\
                line.strip("\n"))
                headerline = line.strip("\n").strip(" ")
            # if header state is true, grab sequence and set dna to
            # true, set header back to False
            elif header and not dna:
                dna = True
                valueline = line.strip("\n").strip(" ")
                header = False
            #if line didn't start with '>' but header is false, then the
            #line is probably another dna-line
            elif not header and dna:
                valueline += line.strip("\n").strip(" ")
            #if none of these are true, valueline and headerline are empty
            #to prevent an unboundlocalerror
            else:
                headerline = ""
                valueline = ""
            count += 1
        # if the values are not empty yet, append to fragments
        # and empty them
        if bool(headerline) and bool(valueline):
            fragments.update({headerline:valueline})
            headerline = ""
            valueline = ""
        opened.close()
        # return dictionary with fasta sequences
        return (fragments, errors)


def csv_open(csvfile):
    """
    opens a csv file and returns a dict (keys=protein name,
    values= tuple with pattern and slicepoint)
    """
    opened = open(csvfile)
    enzymes = {}
    # for each line in opened, seperate lines and append to
    # dictionary
    for line in opened:
        #if csvline is not correct, exit program
        if not csv_check(csvfile, line):
            csvfile = '"' + csvfile + '"'
            print("incorrect csv file:", csvfile, "the file is "\
            "either not the right type or has invalid syntax in it!")
            sys.exit()
        else:
            whole_line = line.split(';')
            enzyme_name = whole_line[0]
            pattern = whole_line[1]
            slicepoint = whole_line[2].strip('\n')
            enzymes.update({enzyme_name: (pattern, slicepoint)})
    opened.close()
    return enzymes


def enzymeparser(enzymes, chosen_enzyme):
    """
    grabs enzymes and selects chosen enzyme out of it. returns dict
    """
    try:
        corenzym = enzymes[chosen_enzyme]
    except KeyError:
        chosen_enzyme = '"' + chosen_enzyme + '"'
        print("enzyme:", chosen_enzyme, "doesnt exist in dictionary!")
        print("available enzymes:")
        for enzyme in enzymes.keys():
            print(enzyme)
        sys.exit()
    return corenzym


def iupac_petterngen(sequence):
    """
    converts pattern from csv file to a RegEx pattern
    """
    pattern = ""
    for letter in sequence:
        pattern += IUPAC_CHARS[letter]

    return pattern


def digest_1_fragment_with_1_enzyme(fragment, enzyme):
    """
    digests 1 fragment with 1 enzyme and
    returns the header and new fragments in a tuple.
    """
    # unpack stuff
    header, sequence = fragment
    pattern = iupac_petterngen(enzyme[0])
    slicepoint = int(enzyme[1])

    # search matches
    matches = re.finditer(pattern, sequence)
    matchpoints = []

    # add match ints to list
    for match in matches:
        matchpoints.append(match.span()[0])

    # if there were no matches, return fragment
    if not matchpoints:

        return fragment
    # else digest found matches.
    else:
        splitcounter = 0
        sequence = list(sequence)
        for matchpoint in matchpoints:
            # splitpoint is the matchinterger and the slicepoint added up
            splitpoint = (matchpoint + slicepoint + splitcounter)
            sequence.insert(splitpoint, " ")
            # each int shifts 1 place each time a digest happens
            splitcounter += 1
        return (header, "".join(sequence).lstrip())


def digest_fragments_with_1_enzyme(fragments, enzyme):
    """
    digests fragments with one enzyme
    returns fragments
    """
    for fragment in fragments.items():
        digested_fragment = digest_1_fragment_with_1_enzyme( \
        fragment, enzyme)
        # update fragments with key and digested_fragment
        # second item in digested_fragment is the new fragment
        # first item in fragment is the key
        fragments[fragment[0]] = digested_fragment[1]
    return fragments


def digest_fragments_with_enzymes(fragments, enzymes, chosen_enzymes):
    """
    digests multiple fragments with multiple enzymes
    and returns fragments
    """
    for chosen_enzyme in chosen_enzymes:
        enzyme = enzymeparser(enzymes, chosen_enzyme)
        fragments = digest_fragments_with_1_enzyme(fragments, enzyme)

    return fragments


def get_molecular_weight(fragments):
    """
    calculates the molmass of each fragment in fragments
    and returns the dictionary with header and list of molmasses
    """
    for fragment in fragments.items():
        mol_fragments = fragment[1].split(" ")
        for i in range(len(mol_fragments)):
            molcounter = 0
            for char in mol_fragments[i]:
                molcounter += MOLMASS_DNA[char]
            mol_fragments[i] = molcounter
        fragments[fragment[0]] = mol_fragments
    return fragments


def print_outcome(molweights):
    """
    prints mol masses in a readable fashion
    molweights[0] = header
    molweights[1] = list with molmasses
    """
    for molweight in molweights.items():
        print(molweight[0])
        print(molweight[1])

    return molweights


def performRestriction(fasta, chosen_enzymes, \
    enzymes=HARD_DICT_ENZYMES, gui=True):
    """
    runs the restriction for the tkinter gui
    name of function should be without caps... mumble mumble
    """
    starttime = time.time()
    fragments, errors = fasta_open(fasta)
    fragments = digest_fragments_with_enzymes(fragments, enzymes, \
    chosen_enzymes)
    molweights = get_molecular_weight(fragments)
    endtime = time.time()
    #print errors / show error pop up
    if bool(errors):
        string = "runtime= "+format(endtime-starttime, '.4f')+\
            " seconds\nUh oh, the program encountered a few errors"+\
            " while running, results may not be accurate.\n"+\
            "The following errors occured:\n\n"+\
            "\n".join(errors)+"\n"
        if gui == False:
            print(string)
        else:

            window = showpop(string)
            window.destroy()
    #print succesful outcome / popup
    else:
        string = "the program ran correctly!\nruntime= "+\
        format(endtime-starttime, '.4f')+" seconds\n"
        if gui == False:
            print(string)
        else:
            window = showpop(string)
            window.destroy()
    return molweights


def argparse_setup():
    """
    sets up argsparse and returns arguments to main
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--FastaPath", help="path to DNA Fasta"\
    " file, if path constains spaces, use quotes around the argument",\
    required=True)
    parser.add_argument("-ef", "--EnzymeFilePath", help="path to"\
    " enzyme CSV file, if path constains spaces, use quotes around"\
    " the argument: '/homes/$USER/my folder with spaces/enzymes.csv'"\
    , required=False)
    parser.add_argument("-e", "--Enzymes", nargs="+", help="enzymes"\
    " you want to digest your fasta with, use spaces between"\
    " enzymes if you want to digest with multiple."\
    " fill in any random garbage if you want a list of enzymes."\
    , required=True)
    args = parser.parse_args()
    # define loose args
    dnapath = args.FastaPath
    enzympath = args.EnzymeFilePath
    enzymes = args.Enzymes

    return [sys.argv[0], dnapath, enzympath, enzymes]


def showpop(string):
    """
    basic window popup when program is finished running
    """
    #string will be place in the window
    window = Tk()
    msg = Label(window, text=string)
    msg.pack()
    button = Button(window, text="Ok", command=window.quit)
    button.pack()
    window.protocol('WM_DELETE_WINDOW', window.quit)
    window.mainloop()
    return window


def main(argv=None):
    """
    this function runs the main program, it lets the user digest a fasta
    sequence with enzymes via the terminal
    usage:
    main([sys.argv[0], dnapath, enzympath, enzymes])
    """
    if argv == None:
        argv = sys.argv

    # work
    #argv[1] = fastafile
    #argv[2] = enzymefile
    #argv[3] = chosenenzymes
    #if enzymepath isnt given, csv_enzymes is a hard dictionary
    if argv[2] == None:
        csv_enzymes = HARD_DICT_ENZYMES
    else:
        csv_enzymes = csv_open(argv[2])

    molweights = performRestriction(argv[1], argv[3],\
    enzymes=csv_enzymes, gui=False)
    print_outcome(molweights)
    return 0


if __name__ == "__main__":
    sys.exit(main(argparse_setup()))
