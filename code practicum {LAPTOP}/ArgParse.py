#!/usr/bin/python3

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-d", "--FastaPath", help="path to DNA Fasta file")
parser.add_argument("-s", "--EnzymeFilePath", help="path to enzyme CSV file")
parser.add_argument("-e", "--Enzyme", help="enzyme")

args = parser.parse_args()


dnapath = args.d
enzympath = args.s
enzyme = args.e
