#!/usr/bin/env python3
"""
program that digests DNA with a range of enzymes
Usage: ./t3.py [enzymefile] [fastafile1,fastafile2, ...] [enzyme1,enzyme2, ...]
"""

#imports
import sys
import re
import argparse

#constants
IUPAC_chars = {
    "A":"A",
    'G':'G',
    'C':'C',
    'T':'T',
    'N':'[ATGC]',
    'R':'[AG]',
    'Y':'[CT]',
    'S':'[GC]',
    'W':'[AT]',
    'K':'[GT]',
    'M':'[AC]',
    'B':'[CGT]',
    'D':'[AGT]',
    'H':'[ACT]',
    'V':'[ACG]',
    '.':' ',
    '-':' '
}

molmass_DNA = {
    'A':135.13,
    'C':111.10,
    'G':151.13,
    'T':126.12
}

hard_dict_enzymes = {
    'EcoRV': ('GATATC', '3'),
    'HinfI': ('GANTCA', '1'),
    'MboI': ('GATC', '0'),
    'XbaI': ('TCTAGA', '1'),
    'EcoRII': ('CCWGG', '0'),
    'HaeIII': ('GGCC', '2'),
    'PstI': ('CTGCAG', '4'),
    'Sau3A': ('GATC', '0'),
    'EcorI': ('GAATTC', '1'),
    'HindIII': ('AAGCTT', '1'),
    'mst2': ('CCTNAGG', '2'),
    'BamHI': ('GGATCC', '1'),
    'NotI': ('GCGGCCGC', '2'),
    'TaqI': ('TCGA', '1')
}

DNA_pattern = "[^ACGT]"
#functions
def fastacheck(fastafile):
    """
    checks if given file is a fasta,
    returns True or False depending on outcome
    """

    if fastafile.endswith(".fa") or fastafile.endswith(".fasta"):
        booltje = True
        opened = open(fastafile)
        for line in opened:
            if line.startswith(">"):
                pass
            else:
                line = line.replace("\n","")
                print(line)
                print('match:',re.search(DNA_pattern,line))
                if re.search(DNA_pattern,line) != None:
                    print('wat')
                    return False
                else:
                    booltje = True
    else:
        booltje = False
    return booltje

def fasta_open(fastafile):
    """
    opens a fasta and returns a dict (keys=header, values=sequences)
    """
    
    if not fastacheck(fastafile):
        print("incorrect fasta file")
        sys.exit()
    else:
        opened = open(fastafile)
        headers = []
        values = []
        header = False
        fragments = {}
        for line in opened:
            #check if line is header, set header state to true
            if line.startswith(">") and not header:
                header = True
                headers.append(line.strip("\n").strip(" "))         
            #if header state is true, grab sequence and append to values, 
            #set header back to False
            elif header:
                values.append(line.strip("\n").strip(" "))
                header = False
        #make a dict filled with keys and values
        for i in range(len(values)):
            fragments.update({headers[i]:values[i]})
        #return list with dicts
        return fragments

def csv_open(csvfile):
    """
    opens a csv file and returns a dict (keys=protein name, 
    values= dict with pattern and slicepoint)
    """
    opened = open(csvfile)
    enzymes = {}
    for line in opened:
        whole_line = line.split(';')
        enzyme_name = whole_line[0]
        pattern = whole_line[1]
        slicepoint = whole_line[2].strip('\n')

        enzymes.update({enzyme_name:(pattern,slicepoint)})
    return enzymes

def enzymeparser(enzymes,chosen_enzyme):
    """
    grabs enzymes and selects chosen enzym out of it. returns dict
    """
    try:
        corenzym = enzymes[chosen_enzyme]
    except:
        print("enzyme doesnt excist in dictionary!")
        sys.exit()
    return corenzym

def IUPAC_patterngen(sequence):
    pattern = ""
    for letter in sequence:
        pattern += IUPAC_chars[letter]
        
    return pattern

def digestOneFragmentWithOneEnzyme(fragment, enzyme):
    """
    digests 1 fragment with 1 enzyme and 
    returns the new fragments in a tuple.
    """
    #unpack stuff
    header, sequence = fragment
    pattern = IUPAC_patterngen(enzyme[0])
    slicepoint = int(enzyme[1])
    
    #search matches
    matches = re.finditer(pattern,sequence)
    matchpoints = []
    
    #add match ints to list
    for match in matches:
        matchpoints.append(match.span()[0])
        
    #if there were no matches, return fragment
    if not matchpoints:
        #~ print("no matches, returning fragment")
        return fragment
    #else digest found matches. 
    else:
        splitcounter = 0
        sequence = list(sequence)
        for matchpoint in matchpoints:
            splitpoint = (matchpoint + slicepoint + splitcounter)
            sequence.insert(splitpoint," ")
            
            splitcounter += 1
        return (header,"".join(sequence))

def digestFragmentsWithOneEnzyme(fragments, enzyme):
    """
    digests fragments with one enzyme
    """
    for fragment in fragments.items():
        digested_fragment = digestOneFragmentWithOneEnzyme(fragment,enzyme)
        #fragment is now a tuple, convert back to fragments.
        fragments[fragment[0]] = digested_fragment[1]
    return fragments

def digestFragmentsWithEnzymes(fragments, enzymes, chosen_enzymes):
    """
    digests multiple fragments with multiple enzymes
    """
    for chosen_enzyme in chosen_enzymes:
        enzyme = enzymeparser(enzymes,chosen_enzyme)
        #~ print(enzyme)
        fragments = digestFragmentsWithOneEnzyme(fragments, enzyme)
    
    return fragments

def getMolecularWeight(fragments):
    for fragment in fragments.items():
        mol_fragments = fragment[1].split(" ")
        for i in range(len(mol_fragments)):
            molcounter = 0
            #~ print("frag: ",mol_fragments[i])
            for char in mol_fragments[i]:
                molcounter += molmass_DNA[char]
            mol_fragments[i] = molcounter
        fragments[fragment[0]] = mol_fragments  
    return fragments

def printertje(molweights):
    """
    prints mol masses in a readable fashion
    """
    for molweight in molweights.items() :
        print(molweight[0])
        print(molweight[1])
    
    return molweights

def performRestriction(fasta, chosen_enzymes):
    fragments = fasta_open(fasta)
    enzymes = hard_dict_enzymes
    fragments = digestFragmentsWithEnzymes(fragments, enzymes, chosen_enzymes)
    molweights = getMolecularWeight(fragments)
    printertje(molweights)
    
    return molweights

def argparse_setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--FastaPath", help="path to DNA Fasta file, if path constains spaces, use quotes around the argument",required=True)
    parser.add_argument("-s", "--EnzymeFilePath", help="path to enzyme CSV file, if path constains spaces, use quotes around the argument",required=True)
    parser.add_argument("-e", "--Enzymes", nargs="+" ,help="enzymes you want to digest your fasta with, use quotes around the argument with multiple enzymes",required=True)
    args = parser.parse_args()
    #define loose args
    dnapath = args.FastaPath
    enzympath = args.EnzymeFilePath
    enzymes = args.Enzymes
    
    return [dnapath,enzympath,enzymes]

def main(argv=None):
    if argv == None:
        argv = sys.argv
    ###work
    fragments = fasta_open(argv[0])
    enzymes = csv_open(argv[1])
    chosen_enzymes = argv[2]
    fragments = digestFragmentsWithEnzymes(fragments, enzymes, chosen_enzymes)
    molweights = getMolecularWeight(fragments)
    printertje(molweights)
    
    return 0

if __name__ == "__main__":
    sys.exit(main(argparse_setup()))
